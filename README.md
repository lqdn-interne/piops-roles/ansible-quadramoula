# ansible-quadramoula


## Description

This role install and configures the Quadramoula donnation website.

## Usage

This role can be used in an Ansible Playbook.

## Requirements

- geerlingguy.nginx
- geerlingguy.nodejs
- geerlingguy.postgresql
- geerlingguy.php

## Role Variables


### Website configuration
`quadramoula_destination: "{{quadramoula_user_home }}/quadramoula"`

### Payement configuration

```
quadramoula_ctx_mode: "TEST" # TEST | PROD
quadramoula_mode: "prod" # prod | dev
quadramoula_app_env: "{{ quadramoula_mode }}"
quadramoula_app_secret: "!changeme!"
quadramoula_secure_scheme: "https" # http | http
quadramoula_site_id: 000000
quadramoula_site_key: 000000
```

### Email
```
quadramoula_from_email: "quadramoula@example.com"
quadramoula_mailer_dsn: "smtpuser:smtppassword@localhost:25"
```
### Source code
```
quadramoula_git_repo: "https://git.laquadrature.net/la-quadrature-du-net/site-de-don/quadramoula.git"
quadramoua_public_web_dir: "{{ quadramoula_destination}}/public"
quadramoula_git_branch: "main"
```
### Unix User 
```
quadramoula_user: "quadramoula"
quadramoula_group: "www-data"
quadramoula_user_home: "/opt/quadramoula"
```
### Admin user
```
quadramoula_admin_user: "admin"
quadramoula_admin_email: "admin@example.org"
quadramoula_admin_fname: "Admin"
quadramoula_admin_lname: "McAdminFace"
quadramoula_admin_password: "!changeme!"
```
### SQL configuration
```
quadramoula_sql_root_password: ""
quadramoula_sql_database_name: ""
quadramoula_sql_username: ""
quadramoula_sql_user_password: ""
quadramoula_sql_connexion_string: "mysql://{{ quadramoula_sql_username }}:{{ quadramoula_sql_user_password }}@127.0.0.1:3306/{{ quadramoula_sql_database_name }}"
```

## Dependencies

roles:
  - name: geerlingguy.nginx
  - name: geerlingguy.nodejs
  - name: geerlingguy.mysql
  - name: geerlingguy.php
## Example Playbook

    - hosts: servers
      roles:
         - lqdn.quadramoula


## Support

See the matrix group over at `#quadramoula:laquadrature.net`


## Contributing

Contributing is welcome, please join the Matrix group and/or open an issue in the Gitlab project.

## Authors and acknowledgment
Written by Nono for La Quadrature Du Net.

## License
AGPLv3

## Project status

In developpement.
